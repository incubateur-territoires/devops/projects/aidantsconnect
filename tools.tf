resource "scaleway_object_bucket" "legacy_zammad_db_backups" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-zammad-db-backups"
}
moved {
  from = module.zammad.scaleway_object_bucket.db_backups
  to   = scaleway_object_bucket.legacy_zammad_db_backups
}
